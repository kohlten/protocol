package events

type ReceivedPacketEvent struct {
	Id uint
	Packet map[string]interface{}
}

type NewClientEvent struct {
	Id uint
}

type ClientDisconnectEvent struct {
	Id uint
}
