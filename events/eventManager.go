package events

import (
    "sync"
)

type Event interface{}

type EventManager struct {
    events      []Event
    eventsMutex *sync.Mutex
}

func NewEventManager() *EventManager {
    em := new(EventManager)
    em.eventsMutex = &sync.Mutex{}
    return em
}

func (em *EventManager) Push(event Event) {
    em.eventsMutex.Lock()
    em.events = append(em.events, event)
    em.eventsMutex.Unlock()
}

func (em *EventManager) Poll() Event {
    em.eventsMutex.Lock()
    if len(em.events) > 0 {
        e := em.events[0]
        em.events = em.events[1:]
        em.eventsMutex.Unlock()
        return e
    }
    em.eventsMutex.Unlock()
    return nil
}

func (em *EventManager) Get(i uint) Event {
    em.eventsMutex.Lock()
    if len(em.events) > 0 && i < uint(len(em.events)) {
        e := em.events[i]
        em.eventsMutex.Unlock()
        return e
    }
    em.eventsMutex.Unlock()
    return nil
}

func (em *EventManager) Remove(i uint) Event {
    em.eventsMutex.Lock()
    if len(em.events) > 0 && i < uint(len(em.events)) {
        em.events = append(em.events[0:i], em.events[i+1:]...)
    }
    em.eventsMutex.Unlock()
    return nil
}

func (em *EventManager) Len() uint {
    return uint(len(em.events))
}

func (em *EventManager) Clear() {
    em.eventsMutex.Lock()
    em.events = nil
    em.eventsMutex.Unlock()
}
