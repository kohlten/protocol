package server

import (
	"fmt"
	"gitlab.com/kohlten/protocol/events"
	"net"
	"sync"
	"syscall"
	"time"
)

type Server struct {
	Events     *events.EventManager
	listener   net.Listener
	idCounter  uint
	clients    map[uint]*client
	clientLock sync.Mutex
	errFunc    func(error) error
	Running    bool
}

func NewServer(listener net.Listener, errFunc func(error) error) *Server {
	s := new(Server)
	s.listener = listener
	s.clients = make(map[uint]*client)
	s.Running = true
	s.errFunc = errFunc
	s.Events = events.NewEventManager()
	go s.accept()
	go s.update()
	return s
}

func (s *Server) Send(id uint, p map[string]interface{}) error {
	client, ok := s.clients[id]
	if !ok {
		return fmt.Errorf("no client with id %d", id)
	}
	return client.send(p)
}

func (s *Server) CloseClient(id uint) error {
	client, ok := s.clients[id]
	if !ok {
		return fmt.Errorf("no client with id %d", id)
	}
	client.close()
	return nil
}

func (s *Server) Close() {
	s.Running = false
	s.clientLock.Lock()
	for _, c := range s.clients {
		c.close()
	}
	s.clientLock.Unlock()
}

func (s *Server) accept() {
	for s.Running {
		conn, err := s.listener.Accept()
		if err != nil {
			if err == syscall.EINVAL {
				s.Close()
				return
			} else {
				continue
			}
		}
		s.clientLock.Lock()
		s.clients[s.idCounter] = newClient(conn, s.errFunc)
		s.clientLock.Unlock()
		event := new(events.NewClientEvent)
		event.Id = s.idCounter
		s.Events.Push(event)
		s.idCounter++
	}
}

func (s *Server) update() {
	for s.Running {
		s.clientLock.Lock()
		for k, c := range s.clients {
			c.update()
			packets := c.getPackets()
			for i := 0; i < len(packets); i++ {
				event := new(events.ReceivedPacketEvent)
				event.Id = k
				event.Packet = packets[i]
				s.Events.Push(event)
			}
			if c.err != nil {
				event := new(events.ClientDisconnectEvent)
				event.Id = k
				s.Events.Push(event)
				c.close()
				delete(s.clients, k)
			}
		}
		s.clientLock.Unlock()
		time.Sleep(1 * time.Millisecond)
	}
}