package server

import (
	"gitlab.com/kohlten/protocol/protocol"
	"net"
)

type client struct {
	p *protocol.Protocol
	err error
}

func newClient(conn net.Conn, errFunc func(error) error) *client {
	c := new(client)
	c.p = protocol.NewProtocol(conn, errFunc)
	return c
}

func (c *client) update() {
	c.err = c.p.GetErr()
}

func (c *client) close() {
	c.p.Close()
}

func (c *client) getPackets() []map[string]interface{} {
	return c.p.GetPackets()
}

func (c *client) send(p map[string]interface{}) error {
	return c.p.Send(p)
}
