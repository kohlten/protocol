package main

import (
	"gitlab.com/kohlten/protocol/client"
	"gitlab.com/kohlten/protocol/events"
	"gitlab.com/kohlten/protocol/util"
	"golang.org/x/exp/errors/fmt"
	"net"
	"time"
)

func main() {
	conn, err := net.Dial("tcp", "0.0.0.0:4444")
	if err != nil {
		panic(err)
	}
	c := client.NewClient(conn, nil)
	ping := util.WrapValues("action", "ping")
	err = c.Send(ping)
	if err != nil {
		panic(err)
	}
	lastTime := util.Timestamp()
	for c.Running {
		if util.Timestamp() - lastTime >= 1000 {
			lastTime = util.Timestamp()
			err = c.Send(ping)
			if err != nil {
				panic(err)
			}
		}
		for event := c.Events.Poll(); event != nil; event = c.Events.Poll() {
			switch e := event.(type) {
			case *events.ReceivedPacketEvent:
				fmt.Println(" Ping took ", util.Timestamp() - lastTime, " ", e.Packet)
			}
		}
		time.Sleep(1 * time.Millisecond)
	}
	c.Close()
}
