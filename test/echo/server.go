package main

import (
	"fmt"
	"gitlab.com/kohlten/protocol/events"
	"gitlab.com/kohlten/protocol/server"
	"net"
	"time"
)

func main() {
	listener, err := net.Listen("tcp", "0.0.0.0:4444")
	if err != nil {
		panic(err)
	}
	s := server.NewServer(listener, nil)
	for s.Running {
		for event := s.Events.Poll(); event != nil; event = s.Events.Poll() {
			switch e := event.(type) {
			case *events.ReceivedPacketEvent:
				fmt.Println("Event from client ", "ReceivedPacketEvent", ":", event)
				err = s.Send(e.Id, e.Packet)
				if err != nil {
					panic(err)
				}
			case *events.ClientDisconnectEvent:
				fmt.Println("Event from client ", "ClientDisconnectedEvent", ":", event)
				s.Close()
			case *events.NewClientEvent:
				fmt.Println("Event from client ", "NewClientEvent", ":", event)
			}
		}
		time.Sleep(1 * time.Millisecond)
	}
	s.Close()
}
