package util

import (
	"time"
)

func Timestamp() uint64 {
	return uint64(time.Now().UnixNano() / int64(time.Millisecond))
}

func WrapValues(args ...interface{}) map[string]interface{} {
	if len(args) % 2 != 0 {
		return nil
	}
	m := make(map[string]interface{})
	for i := 0; i < len(args); {
		if len(args) - i < 2 {
			return nil
		}
		name, ok := args[i].(string)
		if !ok {
			return nil
		}
		m[name] = args[i + 1]
		i += 2
	}
	return m
}