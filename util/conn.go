package util

type Conn interface {
	Send(id uint, p map[string]interface{}) error
}
