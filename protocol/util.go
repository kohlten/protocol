package protocol

func SetMTU(mtu int) {
	MTU = mtu
}

func IsType(data interface{}, t Type) bool {
	switch data.(type) {
	case string:
		return t == TypeString
	case []byte:
		return t == TypeBytes
	case uint64, uint32, uint16, uint8, uint:
		return t == TypeUInt
	case int64, int32, int16, int8, int:
		return t == TypeInt
	case bool:
		return t == TypeBool
	case float32, float64:
		return t == TypeFloat
	}
	return false
}