package protocol

import (
	"bytes"
	"encoding/binary"
	"fmt"
)

func parseHeader(data []byte) (*packet, error) {
	if len(data) < HeaderSize {
		return nil, fmt.Errorf("packet header size is invalid")
	}
	p := new(packet)
	reader := bytes.NewReader(data)
	err := binary.Read(reader, binary.LittleEndian, &p.size)
	if err != nil {
		return nil, err
	}
	err = binary.Read(reader, binary.LittleEndian, &p.sections)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func parsePacket(header *packet, data []byte) (map[string]interface{}, error) {
	if uint32(len(data)) < header.size {
		return nil, fmt.Errorf("packet is not long enough")
	}
	err := parseSections(header, data)
	if err != nil {
		return nil, err
	}
	converted, err := convertSections(header)
	if err != nil {
		return nil, err
	}
	return converted, nil
}

func parseSections(header *packet, data []byte) error {
	reader := bytes.NewReader(data)
	for {
		val := new(value)
		if reader.Len() < ValueHeaderSize {
			return fmt.Errorf("not enough data")
		}
		err := binary.Read(reader, binary.LittleEndian, &val.size)
		if err != nil {
			return err
		}
		err = binary.Read(reader, binary.LittleEndian, &val.t)
		if err != nil {
			return err
		}
		if uint32(reader.Len()) < val.size-ValueHeaderSize {
			return fmt.Errorf("not enough data")
		}
		val.data = make([]byte, val.size-ValueHeaderSize)
		_, err = reader.Read(val.data)
		if err != nil {
			return err
		}
		header.values = append(header.values, val)
		if reader.Len() == 0 || uint16(len(header.values)) == header.sections {
			break
		}
	}
	if uint16(len(header.values)) != header.sections {
		return fmt.Errorf("not enough sections")
	}
	return nil
}

func convertSections(p *packet) (map[string]interface{}, error) {
	m := make(map[string]interface{})
	for i := 0; i < len(p.values); {
		if len(p.values)-i < 2 {
			return nil, fmt.Errorf("not enough sections")
		}
		nameData := p.values[i]
		if nameData.t != TypeString {
			return nil, fmt.Errorf("no name found")
		}
		name, err := convertValue(nameData)
		if err != nil {
			return nil, err
		}
		valData := p.values[i+1]
		val, err := convertValue(valData)
		if err != nil {
			return nil, err
		}
		m[name.(string)] = val
		i += 2
	}
	return m, nil
}

func convertValue(v *value) (interface{}, error) {
	if v.t != TypeString && v.t != TypeBytes {
		val, err := parseValue(v)
		if err != nil {
			return nil, err
		}
		return val, nil
	} else {
		if v.t == TypeString {
			return string(v.data), nil
		}
		return v.data, nil
	}
}

// @TODO STINKYYYYY
func parseType(t Type, b []byte) interface{} {
	var v interface{}
	switch len(b) {
	case 1:
		if t == TypeInt {
			i := int8(0)
			v = &i
		} else if t == TypeBool {
			b := false
			v = &b
		} else {
			i := uint8(0)
			v = &i
		}
	case 2:
		if t == TypeInt {
			i := int16(0)
			v = &i
		} else {
			i := uint16(0)
			v = &i
		}
	case 4:
		if t == TypeInt {
			i := int32(0)
			v = &i
		} else if t == TypeUInt {
			i := uint32(0)
			v = &i
		} else if t == TypeFloat {
			f := float32(0)
			v = &f
		}
	case 8:
		if t == TypeInt {
			i := int64(0)
			v = &i
		} else if t == TypeUInt {
			i := uint64(0)
			v = &i
		} else if t == TypeFloat {
			f := float64(0)
			v = &f
		}
	}
	// V should never be nil
	return v
}

func deref(val interface{}) interface{} {
	switch val.(type) {
	case *bool:
		return *val.(*bool)
	case *int8:
		return *val.(*int8)
	case *int16:
		return *val.(*int16)
	case *int32:
		return *val.(*int32)
	case *int64:
		return *val.(*int64)
	case *uint8:
		return *val.(*uint8)
	case *uint16:
		return *val.(*uint16)
	case *uint32:
		return *val.(*uint32)
	case *uint64:
		return *val.(*uint64)
	case *float32:
		return *val.(*float32)
	case *float64:
		return *val.(*float64)
	}
	return nil
}

// ____________________________________

func parseValue(val *value) (interface{}, error) {
	v := parseType(val.t, val.data)
	reader := bytes.NewReader(val.data)
	err := binary.Read(reader, binary.LittleEndian, v)
	return deref(v), err
}
