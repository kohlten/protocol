package protocol

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"reflect"
)

/*
Format of packets
Header:
| DataSize | Sections |
|  uint32  |  uint16  |
Body:
The body is made up of different sections of this below. The DataSize above is the total size of all the sections.
The name will be a section before the data. So each value will have two of these.
|   Size   |  Type | Data |
|  uint32  | uint8 | ...  |
 */

type packet struct {
	size uint32
	sections uint16
	values []*value
}

type parsedPacket struct {
	header []byte
	body []byte
}

type value struct {
	size uint32
	t Type
	data []byte
}

func newPacket(data map[string]interface{}) (*packet, error) {
	p := new(packet)
	if len(data) == 0 {
		return nil, fmt.Errorf("no values")
	}
	p.sections = uint16(2 * len(data))
	if p.sections > MaxSections {
		return nil, fmt.Errorf("too many values")
	}
	err := p.setValues(data)
	if err != nil {
		return nil, err
	}
	p.setSize()
	return p, nil
}

func (p *packet) setValues(data map[string]interface{}) error {
	for k, v := range data {
		err := p.addValue(k)
		if err != nil {
			return err
		}
		err = p.addValue(v)
		if err != nil {
			return err
		}
	}
	return nil
}

func (p *packet) addValue(val interface{}) error {
	var data []byte
	t := TypeNone
	switch val.(type) {
	case uint:
		val = uint64(val.(uint))
	case int:
		val = int64(val.(int))
	}
	switch val.(type) {
	case uint:
		i, err := p.writeVal(uint64(val.(uint)))
		if err != nil {
			return err
		}
		data = i
		t = TypeUInt
	case uint8, uint16, uint32, uint64:
		i, err := p.writeVal(val)
		if err != nil {
			return err
		}
		data = i
		t = TypeUInt
	case int:
		i, err := p.writeVal(int64(val.(int)))
		if err != nil {
			return err
		}
		data = i
		t = TypeInt
	case int8, int16, int32, int64:
		i, err := p.writeVal(val)
		if err != nil {
			return err
		}
		data = i
		t = TypeInt
	case float32, float64:
		f, err := p.writeVal(val)
		if err != nil {
			return err
		}
		data = f
		t = TypeFloat
	case bool:
		b, err := p.writeVal(val)
		if err != nil {
			return err
		}
		data = b
		t = TypeBool
	case []byte:
		data = val.([]byte)
		t = TypeBytes
	case string:
		data = []byte(val.(string))
		t = TypeString
	default:
		return fmt.Errorf("type %s is not supported", reflect.TypeOf(val).String())
	}
	valStruct := new(value)
	valStruct.data = data
	valStruct.size = uint32(5 + len(data))
	valStruct.t = t
	p.values = append(p.values, valStruct)
	return nil
}

func (p *packet) writeVal(val interface{}) ([]byte, error) {
	write := bytes.NewBuffer(nil)
	err := binary.Write(write, Endian, val)
	if err != nil {
		return nil, err
	}
	return write.Bytes(), nil
}

func (p *packet) setSize() {
	for i := 0; i < len(p.values); i++ {
		p.size += p.values[i].size
	}
}

func (p *packet) toData() *parsedPacket {
	parsed := new(parsedPacket)
	p.makeHeader(parsed)
	p.makeBody(parsed)
	return parsed
}

func (p *packet) makeHeader(parsed *parsedPacket) {
	writer := bytes.NewBuffer(nil)
	_ = binary.Write(writer, binary.LittleEndian, p.size)
	_ = binary.Write(writer, binary.LittleEndian, p.sections)
	parsed.header = writer.Bytes()
}

func (p *packet) makeBody(parsed *parsedPacket) {
	writer := bytes.NewBuffer(nil)
	for _, v := range p.values {
		_ = binary.Write(writer, binary.LittleEndian, v.size)
		_ = binary.Write(writer, binary.LittleEndian, v.t)
		writer.Write(v.data)
	}
	parsed.body = writer.Bytes()
}


