package protocol

import (
	"fmt"
	"gotest.tools/assert"
	"math"
	"testing"
)

func TestPacketNew(t *testing.T) {
	packet, err := newPacket(map[string]interface{}{"int": int32(42), "uint": uint32(42), "float": math.Pi, "string": "hello world", "bytes": []byte("hello world")})
	if err != nil {
		t.Fatal(err)
	}
	parsed := packet.toData()
	assert.Assert(t, len(parsed.header) == HeaderSize)
	assert.Assert(t, uint32(len(parsed.body)) == packet.size)
	header, err := parseHeader(parsed.header)
	if err != nil {
		t.Fatal(err)
	}
	assert.Assert(t, header.size == packet.size)
	assert.Assert(t, header.sections == packet.sections)
	m, err := parsePacket(header, parsed.body)
	if err != nil {
		t.Fatal(err)
	}
	val, ok := m["int"]
	if !ok {
		t.Fatal("value not in output")
	}
	fmt.Println(val.(int32))
}

func TestPacketInvalidType(t *testing.T) {
	_, err := newPacket(map[string]interface{}{"t": []string{"hello"}})
	if err == nil {
		t.Fatal("parsed a invalid type")
	}
}