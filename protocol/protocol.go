package protocol

import (
	"encoding/binary"
	"fmt"
	"gitlab.com/kohlten/protocol/util"
	"net"
	"sync"
	"time"
)

type State int

const (
	StateWaitingForHeader = State(0)
	StateWaitingForData   = State(3)
)

const (
	DefaultWaitTime = 500

	HeaderSize      = 6
	ValueHeaderSize = 5

	MaxDataSize = 0xFFFFFFFF
	MaxSections = 0xFFFF
)

var MTU = 1024

// Types
const (
	TypeNone   = Type(1 << 0)
	TypeInt    = Type(1 << 1)
	TypeUInt   = Type(1 << 2)
	TypeFloat  = Type(1 << 3)
	TypeString = Type(1 << 4)
	TypeBytes  = Type(1 << 5)
	TypeBool   = Type(1 << 6)
)

type Type uint8

type Protocol struct {
	parsedLock    sync.Mutex
	parsed        []map[string]interface{}
	sendQueue     []*packet
	buffer        []byte
	bufferLock    sync.Mutex
	conn          net.Conn
	state         State
	receivedTime  uint64
	currentHeader *packet
	err           error
	errFunc       func(error) error
	running       bool
}

var Endian = binary.LittleEndian

func NewProtocol(conn net.Conn, errFunc func(error) error) *Protocol {
	p := &Protocol{conn: conn, running: true, state: StateWaitingForHeader, errFunc: errFunc}
	go p.read()
	go p.poll()
	return p
}

func (p *Protocol) GetErr() error {
	return p.err
}

func (p *Protocol) InvalidateError() {
	p.err = nil
}

func (p *Protocol) Send(data map[string]interface{}) error {
	packet, err := newPacket(data)
	if err != nil {
		return err
	}
	parsed := packet.toData()
	_, err = p.conn.Write(parsed.header)
	if err != nil {
		p.err = err
		return err
	}
	_, err = p.conn.Write(parsed.body)
	if err != nil {
		p.err = err
		return err
	}
	return nil
}

func (p *Protocol) GetPackets() []map[string]interface{} {
	p.parsedLock.Lock()
	defer p.parsedLock.Unlock()
	packets := p.parsed
	p.parsed = nil
	return packets
}

func (p *Protocol) Close() {
	p.running = false
	p.err = fmt.Errorf("done")
	_ = p.conn.Close()
}

func (p *Protocol) poll() {
	for p.running {
		if p.err != nil {
			return
		}
		for p.parse() == 0 {
		}
		time.Sleep(1 * time.Millisecond)
	}
}

func (p *Protocol) read() {
	for p.running {
		data := make([]byte, MTU)
		dataAvailable, err := p.conn.Read(data)
		if err != nil {
			if p.errFunc != nil {
				err = p.errFunc(err)
			} else {
				err = errFunc(err)
			}
			if err != nil {
				p.err = err
				return
			}
			continue
		}
		if dataAvailable > 0 {
			fmt.Println("Received ", dataAvailable)
			p.bufferLock.Lock()
			p.buffer = append(p.buffer, data[:dataAvailable]...)
			p.bufferLock.Unlock()
		}
	}
}

func errFunc(err error) error {
	val, ok := err.(net.Error)
	if !ok {
		return err
	} else if !val.Timeout() {
		return err
	}
	return nil
}

func (p *Protocol) parse() int {
	if len(p.buffer) == 0 {
		return -1
	}
	if p.state == StateWaitingForHeader {
		p.parseHeader()
	}
	if p.state == StateWaitingForData {
		p.parseBody()
	}
	return 0
}

func (p *Protocol) parseHeader() {
	if len(p.buffer) < HeaderSize {
		return
	}
	p.bufferLock.Lock()
	header, err := parseHeader(p.buffer)
	p.bufferLock.Unlock()
	if err != nil {
		p.err = fmt.Errorf("failed to parse packet from %s. error was %v", p.conn.RemoteAddr(), err)
		return
	}
	p.bufferLock.Lock()
	p.buffer = p.buffer[HeaderSize:]
	p.bufferLock.Unlock()
	p.currentHeader = header
	p.state = StateWaitingForData
	p.receivedTime = util.Timestamp()
}

func (p *Protocol) parseBody() {
	if util.Timestamp()-p.receivedTime > DefaultWaitTime {
		p.currentHeader = nil
		p.state = StateWaitingForHeader
		p.err = fmt.Errorf("client took too long to send data")
		return
	}
	if uint32(len(p.buffer)) < p.currentHeader.size {
		return
	}
	p.bufferLock.Lock()
	body, err := parsePacket(p.currentHeader, p.buffer)
	p.bufferLock.Unlock()
	if err != nil {
		p.err = err
		return
	}
	p.bufferLock.Lock()
	p.buffer = p.buffer[p.currentHeader.size:]
	p.bufferLock.Unlock()
	p.parsedLock.Lock()
	p.parsed = append(p.parsed, body)
	p.parsedLock.Unlock()
	p.state = StateWaitingForHeader
}
