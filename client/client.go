package client

import (
	"gitlab.com/kohlten/protocol/events"
	"gitlab.com/kohlten/protocol/protocol"
	"net"
	"time"
)

type Client struct {
	Events  *events.EventManager
	p       *protocol.Protocol
	addr    string
	Running bool
}

func NewClient(conn net.Conn, errFunc func(error) error) *Client {
	c := new(Client)
	c.addr = conn.RemoteAddr().String()
	c.p = protocol.NewProtocol(conn, errFunc)
	c.Events = events.NewEventManager()
	c.Running = true
	go c.update()
	return c
}

func (c *Client) GetRemoteAddr() string {
	return c.addr
}

// Id has no use here. Only added for compatibility with util.Conn
func (c *Client) Send(id uint, p map[string]interface{}) error {
	return c.p.Send(p)
}

func (c *Client) GetErr() error {
	return c.p.GetErr()
}

func (c *Client) Close() {
	c.Running = false
	c.p.Close()
}

func (c *Client) update() {
	for c.Running {
		if c.p.GetErr() != nil {
			c.Close()
		}
		packets := c.p.GetPackets()
		for i := 0; i < len(packets); i++ {
			event := new(events.ReceivedPacketEvent)
			event.Packet = packets[i]
			c.Events.Push(event)
		}
		time.Sleep(1 * time.Millisecond)
	}
}
